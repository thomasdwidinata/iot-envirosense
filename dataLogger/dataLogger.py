#!/usr/bin/env python3
###
#
# EnviroSense (Data Logger)
# version 1.0
#
# By: Thomas Dwi Dinata
# Student ID : s3702763
#
# Full description available on README.md

from sense_hat import SenseHat
import sqlite3
from pathlib import Path
import logging
import os
from datetime import datetime

# Get home directory to save the database
homeDir = str(Path.home())
defaultDir = homeDir + '/.envirosense'
dbPath = defaultDir + '/data.db'

# For Logging Setup, on the release, it will be deleted
logging.basicConfig(level=logging.DEBUG)

# Create a new instance of SenseHat
SH = SenseHat()

# Sensor Calibration with the Pi's temperature
# vcgencmd tool is to get embedded system's hardware information
# awk tool is to find match pattern and split the string
cpuTemp = os.popen("vcgencmd measure_temp | awk -F '=' '{print $2}'").read()
cpuTemp = cpuTemp.replace("\'C\n", '')

# Get humidity value from SenseHat. The will return the humidity in percentage.
CURRENT_HUMIDITY = SH.get_humidity()

# Get the temperature value from SenseHat. The will return the temperature
# in Celcius. The temperature calibration are as below with factor value of
# 5.466. This can be changed depending on the sensor
# Reference https://github.com/initialstate/wunderground-sensehat/wiki/Part-3.-Sense-HAT-Temperature-Correction
# Formula: { temp_calibrated = temp - ((cpu_temp - temp)/factor) }
# The temperature sensor is calibrated using two sensors, humidity and pressure.
CURRENT_TEMP = (SH.get_temperature() + SH.get_temperature_from_pressure())/2
CURRENT_TEMP = CURRENT_TEMP - ((float(cpuTemp) - CURRENT_TEMP) / 5.466)

# Get pressure value from SenseHat. This will return the pressure in bar
CURRENT_PRESSURE = 0

# Sometimes, sense hat sends value 0, therefore, check it first!
tryout = 0
maxTryOut = 10
while (CURRENT_PRESSURE == 0) and (tryout < maxTryOut):
	CURRENT_PRESSURE = SH.get_pressure()
	tryout += 1

# Debug purpose
logging.debug("Temperature: %s ; Humidity: %s ; Pressure: %s" %
	(CURRENT_TEMP, CURRENT_HUMIDITY, CURRENT_PRESSURE))

# Insert to DB
conn = sqlite3.connect(dbPath)
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS sensors (time DATETIME, temp REAL, hum REAL, pres REAL)")
conn.commit()
# To encode from String to Date Time Object, use this:
# datetime.strptime("2018-08-03 00:06:41", "%Y-%m-%d %H:%M:%S")
currentTime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
c.execute('INSERT INTO sensors(`time`, `temp`, `hum`, `pres`) VALUES("%s", %s, %s, %s)' % (currentTime, CURRENT_TEMP, CURRENT_HUMIDITY, CURRENT_PRESSURE))
conn.commit()
