#!/bin/bash

###
#
# EnviroSense (Installer)
# version 1.0
#
# By: Thomas Dwi Dinata
# Student ID : s3702763
#
# Full description available on README.md
#

# Check whether it is root
if [[ $EUID -ne 0 ]]; then
  echo "You are not running as root. Please use sudo or login as root!";
  exit;
fi

USER_HOME=$(eval echo ~${SUDO_USER})
INSTALL_PATH=$USER_HOME

# Concat the path with the default envirosense folder
INSTALL_PATH="$INSTALL_PATH/.envirosense"

echo "EnviroSense"
echo "Version 1.0 Beta 6"
echo "This script will install EnviroSense Version 1.0 Beta 6 to $INSTALL_PATH."
read -p "Are you sure [y/n]? " -n 1 -r
echo ""
if [[ ! $REPLY =~ ^[Yy]$ ]];
then
  echo "Abort."
  [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

# Check Dirs
REQUIRED_FOLDERS="./bluetooth ./dataLogger ./monitor ./server ./server/static/*"
set -- $REQUIRED_FOLDERS
DIRS="$@"
for i in "$@"
do
  if [ ! -d $i ]; then
    echo "One of the directory is not exist! It is called: $i. Exiting...";
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
  fi
done

# Check files inside Dirs
REQUIRED_FILES=("bluetoothNearby.py"
  "dataLogger.py"
  "monitor.py"
  "server.py"
)

REQUIRED_FILES_WITH_PATH=("./bluetooth/${REQUIRED_FILES[0]}"
  "./dataLogger/${REQUIRED_FILES[1]}"
  "./monitor/${REQUIRED_FILES[2]}"
  "./server/${REQUIRED_FILES[3]}"
)
for i in "${REQUIRED_FILES_WITH_PATH[@]}";
do
  if ! test -f "$i"; then
    echo "One of the required files are missing! Which is called $i. Exiting...";
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
  fi
done

# Check whether SQLite has been installed or not
# https://linuxconfig.org/how-to-test-for-installed-package-using-shell-script-on-ubuntu-and-debian
dpkg -s "sqlite3" &> /dev/null
if [ ! $? -eq 0 ]; then
  read -p "SQLite3 is not installed on your machine. Do you want to install it [y/n]? " -n 1 -r
  echo ""
  if [[ ! $REPLY =~ ^[Yy]$ ]];
  then
    echo "Abort."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
  else
    apt install sqlite3
  fi
fi

# ==============================================================================
# If all pass, proceed to installation
echo "All required files are present. Proceeding the installation to ./envirosense"

# Create Directory
rm -dfr "${INSTALL_PATH}"
mkdir "${INSTALL_PATH}"
chmod 777 "${INSTALL_PATH}"

# Copy all python scripts to destination folder
for i in "${REQUIRED_FILES_WITH_PATH[@]}";
do
  echo "Copying $i to $INSTALL_PATH/$CURRENT_FILE ..."
  cp $i $INSTALL_PATH/$CURRENT_FILE
done

echo "Copying ./server/index.tpl to $INSTALL_PATH/index.tpl ..."
cp "./server/index.tpl" "${INSTALL_PATH}/index.tpl"
echo "Copying ./server/static folder to $INSTALL_PATH/static ..."
cp -r "./server/static" "${INSTALL_PATH}/static"

read -p "Do you want EnviroSense server to automatically start [y/n]? " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]];
then
  # Add to /etc/init.d/
  i="./server/init.d-script"
  if test -f "$i"; then
    cp "./server/init.d-script" "/etc/init.d/serverEnviroSense"
    chmod 755 "/etc/init.d/serverEnviroSense"
    update-rc.d serverEnviroSense defaults
    update-rc.d serverEnviroSense enable
    service serverEnviroSense start
  else
    echo "One of the required files are missing! Which is called $i. Skipping...";
  fi
fi

read -p "Do you want to add EnviroSense monitoring in schedule for every 15 minutes? Select 'N' if you want to set it up later. You can configure this using the EnviroSense Web Interface later [y/n]? " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]];
then
  # Add to Crontab
  crontab -l -u $SUDO_USER > .cronjob.bak
  echo "*/15 * * * * python3 ${INSTALL_PATH}/dataLogger.py >> ${INSTALL_PATH}/dataLoggerLog.txt # envirosense-dataLogger" >> .cronjob.bak
  echo "*/15 * * * * python3 ${INSTALL_PATH}/monitor.py >> ${INSTALL_PATH}/dataLoggerLog.txt # envirosense-pushMonitor" >> .cronjob.bak
  crontab .cronjob.bak -u $SUDO_USER
  rm .cronjob.bak
else
  echo "Skipping Crontab installation..."
fi

echo -n "Please enter your name: "
read OWNER_NAME
echo -n "Please enter your Pushbullet API Key (It will be saved on ${INSTALL_PATH}/.envirosense/pushbullet.txt): "
read PUSHBULLET_API
echo -n "Please enter one of your own device Bluetooth Name (It will be saved on ${INSTALL_PATH}/.envirosense/bluetoothPairs): "
read BLUETOOTH_NAME

echo "${OWNER_NAME}" > "${INSTALL_PATH}/name"
echo "${PUSHBULLET_API}" > "${INSTALL_PATH}/pushbullet.txt"
echo "${INSTALL_PATH}/dataLogger.py" > "${INSTALL_PATH}/scriptPath.txt"
echo "${INSTALL_PATH}/monitor.py" >> "${INSTALL_PATH}/scriptPath.txt"
echo "${BLUETOOTH_NAME}" > "${INSTALL_PATH}/bluetoothPairs"

echo "Installation finished!"
