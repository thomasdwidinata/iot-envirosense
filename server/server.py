#!/usr/bin/env python3
###
#
# EnviroSense (Web Server)
# version 1.0.1
#
# By: Thomas Dwi Dinata
# Student ID : s3702763
#
# Full description available on README.md

from bottle import route, run, template, Bottle, response, static_file, request
import sqlite3
import logging
import json
import ctypes
import os
import sys
import getpass
import math
from crontab import CronTab
from pathlib import Path
import socket
from sense_hat import SenseHat
from threading import Thread
from time import sleep

SH = SenseHat()
SH.clear(255,255,255)
portEighty = False

if len(sys.argv) > 1:
    if "--defaultHttp" in sys.argv:
        portEighty = True

homeDir = str(os.path.expanduser('~pi'))
defaultDir = homeDir + "/.envirosense"
databasePath = defaultDir + '/data.db'
serverLog = defaultDir + "/serverLog.txt"
logging.basicConfig(filename=serverLog, level=logging.INFO)

# Checks whether directory exist or not, if not
if not os.path.isdir(defaultDir):
    try:
        os.makedirs(defaultDir) # Create the directory
    except Exception: # If not permitted, log it, and exit!
        logging.critical("Unable to create default directory on %s!" % defaultDir)
        exit(-1)

filePtr = open(defaultDir + '/scriptPath.txt', 'r') # Create file pointer to read the file
dataLoggerScript = filePtr.readline().replace('\n','') # Read first line, contains datalogger script
monitorScript = filePtr.readline().replace('\n','') # Read second line, contains pushbullet script
filePtr.close() # Don't forget to close the file to release the lock file

dataLoggerLogFile = defaultDir + '/dataLoggerLog.txt'
monitorLogFile = defaultDir + '/monitorLog.txt'
username = getpass.getuser()

ip = "0.0.0.0" # Not loopback, instead to public
port = 8080

try: # Try whether currently Root Access on UNIX/Linux
    is_admin = os.getuid() == 0
except AttributeError: # If not UNIX/Linux, try Windows mode
    is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0
    logging.critical("Windows is not supported. Aborting...")
    print("Windows is not supported. Aborting...")
    exit(-255)
except: # Unsupported system, not UNIX/Linux or Windows. Might be Cygwin under Windows. Set to False
    is_admin = False

if is_admin and portEighty:
    port = 80
    logging.info("Root access gained and --defaultHttp specified. Using Port 80 for the Web server!")
else:
    logging.warning("Port 80 selected, but not running as Root. Will now still using 8080.")

SH.clear(0,0,0)

def startupDisplay():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(("8.8.8.8", 80))
    Ip = sock.getsockname()[0]
    sock.close()
    SH.show_message("EnviroSense", text_colour=[107, 163, 255])
    SH.show_message("%s:%s" % (Ip, port), text_colour=[137, 255, 206])

displayThread = Thread(target = startupDisplay, args=())
displayThread.start()

conn = sqlite3.connect(databasePath)
c = conn.cursor()

# Making sure table exists, if not, it will raises an error. So make it!
c.execute("CREATE TABLE IF NOT EXISTS sensors (time DATETIME, temp REAL, hum REAL, pres REAL)")
conn.commit()

def codeGenerator(code, message):
    codeGen = {
        "code": code,
        "message": message
    }
    return json.dumps(codeGen)

app = Bottle()

# Used for accessing the main page assets
@app.get("/static/css/<filepath:re:.*\.(css|map)>")
def css(filepath):
    return static_file(filepath, root="static/css")
@app.get("/static/img/<filepath:re:.*\.(svg|png|jpg)>")
def img(filepath):
    return static_file(filepath, root="static/img")
@app.get("/static/js/<filepath:re:.*\.(js|map)>")
def js(filepath):
    return static_file(filepath, root="static/js")
# This will be Vue.js
@app.route('/', method=['GET'])
def index():
    return template('index')

# API Handling
@app.route('/api/fetchAll', method=['GET'])
def fetchAll():
    c.execute("SELECT * FROM sensors")
    data = []
    for a in c.fetchall():
        data.append({"time": a[0], "temperature":a[1], "humidity":a[2], "pressure":a[3]})
    return json.dumps(data)

@app.route('/api/fetch/<itemPerGraph>/<page>')
def fetchSome(itemPerGraph, page):
    startSkip = (int(itemPerGraph) * (int(page) - 1))
    # SQL Server Injection Prevention, by using sqlite3's default library
    c.execute("SELECT * FROM sensors LIMIT ?, ?", (str(startSkip), str(itemPerGraph)))
    data = []
    for a in c.fetchall():
        data.append({"time": a[0], "temperature":a[1], "humidity":a[2], "pressure":a[3]})
    return json.dumps(data)

@app.route('/api/config/sensorInterval/', method=['OPTIONS', 'POST']) # This should be in seconds
def intervalSet():
    # Handling CORS, just ignore to make is simple
    if request.method == 'OPTIONS':
        return {};

    owner = request.forms.get('ownerName')
    logger = request.forms.get('dataLoggerFrequency')
    monitor = request.forms.get('pushMonitorFrequency')
    if (math.floor(int(logger)) <= 0) and (math.floor(int(monitor)) <= 0):
        return codeGenerator(-1, "Unable to set for every 0 minutes") # For now, hard coded
    if owner:
        try:
            namePtr = open(defaultDir + '/name', 'w') # Replace the file
            namePtr.write(owner)
            print("pass")
            namePtr.close()
        except IOError:
            return codeGenerator(-100, "Unable to save owner's name! Reinstall EnviroSense to rebuild the application configuration")
    sensorCron = CronTab(user=username)
    found = 0 # 0 means not found, 1 means only found one, 2 means all has been found
    for job in sensorCron:
        if job.comment == 'envirosense-dataLogger':
            job.clear()
            job.minute.every(math.floor(int(logger)))
            found += 1
        elif job.comment == 'envirosense-pushMonitor':
            job.clear()
            job.minute.every(math.floor(int(monitor)))
            found +=1
    if found != 2:
        sensorCron.remove_all()
        dataLogger = sensorCron.new(command="python3 " + dataLoggerScript + " >> " + dataLoggerLogFile, comment="envirosense-dataLogger")
        monitorCron = sensorCron.new(command="python3 " + monitorScript + " >> " + monitorLogFile, comment="envirosense-pushMonitor")
        dataLogger.minute.every(math.floor(int(logger)))
        monitorCron.minute.every(math.floor(int(monitor)))
    sensorCron.write()
    return codeGenerator(0, "OK")

@app.route('/api/config/sensorInterval', method=['GET'])
def getConfig():
    arr = {
        "dataLoggerFrequency":0,
        "pushMonitorFrequency":0,
        "ownerName":""
    }
    dataLoggerFrequency = 0
    pushMonitorFrequency = 0
    crons = CronTab(user=username)
    for job in crons:
        if job.comment == 'envirosense-dataLogger':
            dataLoggerFrequency = str(job.minute).split('/')[1]
        elif job.comment == 'envirosense-pushMonitor':
            pushMonitorFrequency = str(job.minute).split('/')[1]
    namePtr = open(defaultDir + '/name', 'r')
    name = namePtr.readline().replace('\n','')
    arr['dataLoggerFrequency'] = dataLoggerFrequency
    arr['pushMonitorFrequency'] = pushMonitorFrequency
    arr['ownerName'] = name
    return json.dumps(arr)

@app.route('/api/maxPage/<dataPerPage>')
def getPage(dataPerPage):
    c.execute("SELECT ROUND(COUNT(*))/? FROM sensors;", (dataPerPage, ))
    page = math.ceil(c.fetchone()[0])
    pageCount = {
        'pageCount': page
    }
    return json.dumps(pageCount)

# CORS Protection Header Injection
@app.hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

run(app, host=ip, port=port)
