import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import History from '@/components/History'
import Configuration from '@/components/Configuration'
import PageNotFound from '@/components/PageNotFound'

Vue.use(Router)

export default new Router({
  routes: [
    // Dashboard
    {
      'path': '/',
      'name': 'Dashboard',
      'component': Dashboard
    },
    // History
    {
      'path': '/history',
      'name': 'History',
      'component': History
    },
    // Configuration
    {
      'path': '/config',
      'name': 'Configuration',
      'component': Configuration
    },
    // Main 404 Not Found page
    {
      'path': '/404',
      'name': '404 Not Found',
      'component': PageNotFound
    },
    // Any other URL will redirected into 404 Not Found
    {
      'path': '*',
      'redirect': '/404'
    }
  ]
})
