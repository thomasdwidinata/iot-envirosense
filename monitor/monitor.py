#!/usr/bin/env python3
###
#
# EnviroSense (Pushbullet Monitor)
# version 1.0
#
# By: Thomas Dwi Dinata
# Student ID : s3702763
#
# Full description available on README.md

from sense_hat import SenseHat
import time
import logging
from pushbullet import Pushbullet
from pathlib import Path

homeDir = str(Path.home())
SH = SenseHat()

coldColour = [66, 134, 244]
okColour = [255, 202, 117]
hotColour = [255, 103, 15]

filePath = homeDir + '/.envirosense'
apiPath = filePath + '/pushbullet.txt'
tempPath = homeDir + '/lastTemp'

LAST_TEMP = 28 # Default Value

# Read Pushbullet API Key
try:
    filePtr = open(apiPath) # Create file pointer to read the file
    apiKey = ""
    apiKey = filePtr.read().replace('\n','') # Read the API key, and remove the newline
    if not apiKey:
        logging.error("API Key is not present in %s. Please add the API key there!" % apiPath)
        exit(-2)
    filePtr.close() # Close the pointer to release the lock
except:
    logging.critical("No API key file was found on %s! Please create a new file and save the API key there." % apiPath)
    print("No API key file was found on %s! Please create a new file and save the API key there." % apiPath)
    exit(-1)

# Read last temperature
try:
    filePtr = open(tempPath, 'r')
    LAST_TEMP = int(filePtr.read().replace('\n',''))
    filePtr.close()
except:
    filePtr = open(tempPath, 'w')
    filePtr.write(str(LAST_TEMP))
    filePtr.close()

pb = Pushbullet(apiKey)

def displayMessage(iterateable, colour):
    iteration = 0
    while iteration < len(iterateable):
        SH.show_letter(iterateable[iteration], colour)
        iteration += 1
        time.sleep(0.5)

temp = SH.get_temperature()
try:
    filePtr = open(tempPath, 'w')
    filePtr.write(str(temp))
    filePtr.close()
except:
    print("Unable to save latest temperature... Make sure you have access to %s" % (tempPath))
    logging.critical("Unable to save latest temperature... Make sure you have access to %s" % (tempPath))
if (temp < 25):
    if (LAST_TEMP >= 25):
        push = pb.push_note("EnviroSense", "Your room temperature drops to %s\
        degree. You might want to bring your jacket!" % str(temp))
    displayMessage("COLD!", coldColour)
elif temp > 35:
    if (LAST_TEMP <= 35):
        push = pb.push_note("EnviroSense", "Your room temperature raises to %s\
        degree. You might want to turn on the air conditioner!" % str(temp))
    displayMessage("HOT!", hotColour)
else:
    print("Good, room temp is %s" % str(temp))
    displayMessage("OK", okColour)
SH.clear()
