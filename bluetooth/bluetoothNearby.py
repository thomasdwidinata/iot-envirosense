#!/usr/bin/env python3
###
#
# EnviroSense (Bluetooth Monitor)
# version 1.0.1
#
# By: Thomas Dwi Dinata
# Student ID : s3702763
#
# Full description available on README.md

from sense_hat import SenseHat
import bluetooth
import time
import logging
import os
from pathlib import Path
from pushbullet import Pushbullet

logging.basicConfig(filename="/home/pi/bluetoothLog.txt", level=logging.INFO)

coldColour = [66, 134, 244]
okColour = [255, 202, 117]
hotColour = [255, 103, 15]

homeDir = str(Path.home())
defaultDir = homeDir + "/.envirosense"
# Checks whether directory exist or not, if not
if not os.path.isdir(defaultDir):
    try:
        os.makedirs(defaultDir) # Create the directory
    except Exception: # If not permitted, log it, and exit!
        logging.critical("Unable to create default directory on %s!" % defaultDir)
        exit(-1)

def blink():
    SH.clear(137, 211, 255)
    time.sleep(0.2)
    SH.clear(0,0,0)
    time.sleep(0.2)
    SH.clear(137, 211, 255)
    time.sleep(0.2)
    SH.clear(0,0,0)
    time.sleep(0.2)
    SH.clear(137, 211, 255)
    time.sleep(0.2)
    SH.clear(0,0,0)
    time.sleep(0.2)
    SH.clear(137, 211, 255)
    time.sleep(0.2)
    SH.clear(0,0,0)
    time.sleep(0.2)
    SH.clear(137, 211, 255)
    time.sleep(0.5)
    SH.clear(0,0,0)

def displayMessage(iterateable, colour):
    if not isinstance(iterateable, list):
        return
    iteration = 0
    while iteration < len(iterateable):
        SH.show_letter(iterateable[iteration], colour)
        iteration += 1
        time.sleep(0.5)

bluetoothDevices = []
name = "pi"
api = ""
APIpath = homeDir + '/.envirosense/pushbullet.txt'
SH = SenseHat()
push = ""

# Load additional data
try:
    namePtr = open(defaultDir + '/name', 'r')
    apiPtr = open(APIpath, 'r')
    api = apiPtr.readline().replace('\n', '')
    name = namePtr.readline().replace('\n', '')
    if not api:
        logging.error("API Key is not present in %s. Please add the API key there!" % APIpath)
        exit(-2)
    apiPtr.close()
    namePtr.close()
    pb = Pushbullet(api)
except Exception:
    logging.error("Unable to load your Name or API Key! Aboring!")
    exit(-1)

# Loop and insert to Array
try:
    with open(defaultDir + '/bluetoothPairs', 'r') as lines:
        for line in lines:
            bluetoothDevices.append(line.replace('\n',''))
except:
    print("Unable to read Bluetooth Devices List! Please list down all of your devices on %s/bluetoothPairs" % defaultDir)
    logging.error("Unable to read Bluetooth Devices List! Aborting!")
    exit(-1)

loops = 1
maxTry = 5

for a in range(maxTry+1):
    MAC = None
    date = time.strftime("%y %b %d %H:%M:%S", time.localtime())

    near = bluetooth.discover_devices()
    found = False
    deviceName = ''

    for macAddr in near:
        for devName in bluetoothDevices:
            if devName == bluetooth.lookup_name(macAddr, timeout=5):
                MAC = macAddr
                deviceName = devName
                found = True
                break
        if found:
            break

    if MAC is not None:
        blink()
        push = pb.push_note("EnviroSense: Bluetooth", "Hello! One of your device are nearby the Pi! It is called: %s" % deviceName)
        logging.info("{} found!".format(devName))
        temp = round(SH.get_temperature(), 1)
        SH.show_message("Hi {}! Current Temp is".format(name))
        if temp < 25:
            SH.show_message("{}*C".format(temp), text_colour=coldColour)
        elif temp > 35:
            SH.show_message("{}*C".format(temp), text_colour=hotColour)
        else:
            SH.show_message("{}*C".format(temp), text_colour=okColour)
        SH.show_message("Found device name: {}.".format(deviceName))
        loops = 6
    else:
        SH.clear()
        SH.show_letter('?', text_colour=[200,0,0], back_colour=[200,200,200])
        time.sleep(1)
        SH.show_message('Try: {}/{}'.format(str(loops), str(maxTry)), text_colour=[255, 146, 5], back_colour=[0, 106, 168])
        print("Could not find target device nearby... Trying {}/{}".format(loops, maxTry))
        loops += 1

if loops > 5:
    print("Give up. No devices found!")
    SH.show_message("No devices found...", text_colour=[255,0,0])
