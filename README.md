# Programming Internet of Things (COSC 2674)

## IoT EnviroSense / Assignment 1

![EnviroSense Icon](./icon/iot-envirosense-small.png)

### Description
The purpose of this assignment is to familiarise students with Raspberry Pi and Sense Hat. The aim of this project is to create a mini IoT application which is temperature and humidity data recording and notifier. The Pi will record the humidity and the temperature value got from the Sense Hat and save it every hour. If the room temperature drops to less than 20 degrees, the Pi will remind the user by giving push notification to bring a sweater to the user's phone. Another functionality is to use a bluetooth hardware built into the Pi. When the Pi found known bluetooth device, it will greet the person and give the current temperature.

### Technologies, Languages, and Dependencies
The machine that we are using on this Project are:
* Raspberry Pi 3 Model B
* Raspberry Pi Sense Hat

The programming language / libraries that we are using is :
* Python 3 (v3.7.0:1bf9cc5093 on MacOS and 3.5.3 on Raspbian)
* `UIKit` - A lightweight and modular front-end framework for developing fast and powerful web interfaces
* `Vue.JS` - The Progressive JavaScript Framework
* `C3.js` - D3-based reusable chart library
* `sense_hat` - SenseHat library to access Raspberry Pi's SenseHat device
* `bottle` - Web framework
* `pylint` - Debugger tool
* `sqlite3` - Database management system
* `python-crontab` - Crontab manager
* `pushbullet.py` - Pushbullet Library
* `pybluez` - Bluetooth Library

All of this source code are debugged using `pylint 2.1.0` & `astroid 2.0.2` on `Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 26 2018, 23:26:24) [Clang 6.0 (clang-600.0.57)]` (MacOS) and `pylint 2.1.0` & `astroid 2.0.2 on `Python 3.5.3 (default, Jan 19 2017, 14:11:04) [GCC 6.3.0 20170124]` (Raspbian).

We will be running this Pi in headless mode, which means does not uses any external monitor and even no GUI installed on the OS itself. The wireless access has been configured on the `/etc/wpa_supplicant/wpa_supplicant.conf`.

### Installation
To install EnviroSense into your Raspberry Pi, you need to clone this repository, then run `install.sh` script. The script requires root privileges, make sure you run it using `sudo`. The it will automatically install it to `/home/pi/.envirosense` folder. It will copy required files such as the bluetooth module, dataLogger module, pushMonitor module, and the server to serve the web interface. The script will ask whether the server should be runned automatically or not.
Later, the script will ask your name, Pushbullet API key, and your device bluetooth name. You can modify this on `/home/pi/.envirosense` file.
To configure the frequency of data logging, simply go to the web interface (The web interface serves at port 8080), then go to Configuration. You can set both of data logging and push monitor frequency in minutes.

### Web Server

The web server framework that we are using is called `Bottle` framework. Most of the Pi's job is only serve the API to access the logging data. The main page is rendered using Vue.JS, a Javascript framework, and will be rendered and processed on the client's side. The server (Raspberry Pi), will serve the Vue.JS page, and API. The Vue.JS will request data API to Bottle Framework and Bottle will return the data to Vue.JS.

### Branches

There are three branches on this Git repository. Below are the description of each branches:
* `development` : Development Snapshot
* `release` : The release version, marking the version upgrade/update
* `master` : Currently deployed and tested on the Raspberry Pi machine
All development snapshots are recorded here

### Reference

Some of this project source code were inspired and helped by several communities around the world. Here are the references including where does it taken from:
* `/dataLogger/dataLogger.py` - [Sense HAT Temperature Correction](https://github.com/initialstate/wunderground-sensehat/wiki/Part-3.-Sense-HAT-Temperature-Correction)
* `/bluetooth/bluetoothNearby.py` and `/monitor/monitor.py` - [pushbullet.py Documentation](https://github.com/randomchars/pushbullet.py)
* `/server/vuejs/src/components/Dashboard.vue` - [Convert MySql DateTime stamp into JavaScript's Date format](https://stackoverflow.com/questions/3075577/convert-mysql-datetime-stamp-into-javascripts-date-format)
* `/server/server.py` - [Bottle Tutorial](https://bottlepy.org/docs/dev/tutorial.html)
* `/server/server.py` - [Bottle with Cross-origin resource sharing (CORS)](https://gist.github.com/richard-flosi/3789163)
* [C3.js Timeseries Chart](https://c3js.org/samples/timeseries.html)
* [VueJS Webpack](https://github.com/vuejs-templates/webpack)
* [Debian TimeZoneChanges](https://wiki.debian.org/TimeZoneChanges)

### Assignment Rubric Achievement

To comply with the course assignment specification and rubric, here are some clarification to simplify the assessment:
* The debugging code are mostly now saved into Logging file, which can be read on `~/.envirosense` folder
* This project uses 3 branches. See Branches section for more information.
* Since the source code of `/dataLogger/dataLogger.py` is used to save the temperature and humidity value, the SQL injection has been implemented to prevent sensor tampering.
* The Web Server is using `Bottle` microframework, and it only serve API. See Web Server section for more details. The data representation is displayed and rendered using C3.js and has different colour when representing different sensor values.
* The pushbullet works when the API key is specified on `~/.envirosense/pushbullet.txt`. The tilda (~) is the home directory. You can find your home directory by typing `echo $HOME`. The pushbullet will push only if the previous value is more than 25°C.
* The known bluetooth devices will be stored in `~/.envirosense/bluetoothPairs`. How the script works is by scanning all bluetooth devices nearby and then it will traverse all of the bluetooth device names and compare whether your devices are nearby. If found, then it will display hi to you with your name (Stored in `~/.envirosense/name`), with colourful text displaying current temperature.
* The Server Code handles SQL Injection build in, thanks to SQLite3 library that has already implement it natively.

### Changes

#### Version 1.0.1
* Bug fixes on Server code, which returns wrong data on pagination request

#### Version 1.0r1
* Fixed security issue on SQL Injection on Server Code

#### Version 1.0
* All functions successfully implemented

#### Version 1.0 Beta 3
* Automated Data Logger and Environment Monitor and push using Pushbullet using cron
* Web dashboard is now available

#### Version 0.1 Alpha 1
* Sensors value and Database accessing are available now
